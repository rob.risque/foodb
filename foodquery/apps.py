from django.apps import AppConfig


class FoodqueryConfig(AppConfig):
    name = 'foodquery'
