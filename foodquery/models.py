# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CompoundAlternateParents(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    compound = models.ForeignKey('Compounds', models.DO_NOTHING, blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'compound_alternate_parents'


class CompoundExternalDescriptors(models.Model):
    external_id = models.CharField(max_length=255, blank=True, null=True)
    annotations = models.CharField(max_length=255, blank=True, null=True)
    compound = models.ForeignKey('Compounds', models.DO_NOTHING, blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'compound_external_descriptors'


class CompoundSubstituents(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    compound = models.ForeignKey('Compounds', models.DO_NOTHING, blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'compound_substituents'


class CompoundSynonyms(models.Model):
    synonym = models.CharField(unique=True, max_length=255)
    synonym_source = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    source_id = models.IntegerField(blank=True, null=True)
    source_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'compound_synonyms'


class Compounds(models.Model):
    legacy_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=255)
    public_id = models.CharField(unique=True, max_length=9)
    name = models.CharField(unique=True, max_length=255)
    export = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    annotation_quality = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    cas_number = models.CharField(max_length=255, blank=True, null=True)
    melting_point = models.TextField(blank=True, null=True)
    protein_formula = models.CharField(max_length=255, blank=True, null=True)
    protein_weight = models.CharField(max_length=255, blank=True, null=True)
    experimental_solubility = models.CharField(max_length=255, blank=True, null=True)
    experimental_logp = models.CharField(max_length=255, blank=True, null=True)
    hydrophobicity = models.CharField(max_length=255, blank=True, null=True)
    isoelectric_point = models.CharField(max_length=255, blank=True, null=True)
    metabolism = models.TextField(blank=True, null=True)
    kegg_compound_id = models.CharField(max_length=255, blank=True, null=True)
    pubchem_compound_id = models.CharField(max_length=255, blank=True, null=True)
    pubchem_substance_id = models.CharField(max_length=255, blank=True, null=True)
    chebi_id = models.CharField(max_length=255, blank=True, null=True)
    het_id = models.CharField(max_length=255, blank=True, null=True)
    uniprot_id = models.CharField(max_length=255, blank=True, null=True)
    uniprot_name = models.CharField(max_length=255, blank=True, null=True)
    genbank_id = models.CharField(max_length=255, blank=True, null=True)
    wikipedia_id = models.CharField(max_length=255, blank=True, null=True)
    synthesis_citations = models.TextField(blank=True, null=True)
    general_citations = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    protein_structure_file_name = models.CharField(max_length=255, blank=True, null=True)
    protein_structure_content_type = models.CharField(max_length=255, blank=True, null=True)
    protein_structure_file_size = models.IntegerField(blank=True, null=True)
    protein_structure_updated_at = models.DateTimeField(blank=True, null=True)
    msds_file_name = models.CharField(max_length=255, blank=True, null=True)
    msds_content_type = models.CharField(max_length=255, blank=True, null=True)
    msds_file_size = models.IntegerField(blank=True, null=True)
    msds_updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    phenolexplorer_id = models.IntegerField(blank=True, null=True)
    dfc_id = models.CharField(max_length=255, blank=True, null=True)
    hmdb_id = models.CharField(max_length=255, blank=True, null=True)
    duke_id = models.CharField(max_length=255, blank=True, null=True)
    drugbank_id = models.CharField(max_length=255, blank=True, null=True)
    bigg_id = models.IntegerField(blank=True, null=True)
    eafus_id = models.CharField(max_length=255, blank=True, null=True)
    knapsack_id = models.CharField(max_length=255, blank=True, null=True)
    boiling_point = models.CharField(max_length=255, blank=True, null=True)
    boiling_point_reference = models.CharField(max_length=255, blank=True, null=True)
    charge = models.CharField(max_length=255, blank=True, null=True)
    charge_reference = models.CharField(max_length=255, blank=True, null=True)
    density = models.CharField(max_length=255, blank=True, null=True)
    density_reference = models.CharField(max_length=255, blank=True, null=True)
    optical_rotation = models.CharField(max_length=255, blank=True, null=True)
    optical_rotation_reference = models.CharField(max_length=255, blank=True, null=True)
    percent_composition = models.CharField(max_length=255, blank=True, null=True)
    percent_composition_reference = models.CharField(max_length=255, blank=True, null=True)
    physical_description = models.TextField(blank=True, null=True)
    physical_description_reference = models.TextField(blank=True, null=True)
    refractive_index = models.CharField(max_length=255, blank=True, null=True)
    refractive_index_reference = models.CharField(max_length=255, blank=True, null=True)
    uv_index = models.CharField(max_length=255, blank=True, null=True)
    uv_index_reference = models.CharField(max_length=255, blank=True, null=True)
    experimental_pka = models.CharField(max_length=255, blank=True, null=True)
    experimental_pka_reference = models.CharField(max_length=255, blank=True, null=True)
    experimental_solubility_reference = models.CharField(max_length=255, blank=True, null=True)
    experimental_logp_reference = models.CharField(max_length=255, blank=True, null=True)
    hydrophobicity_reference = models.CharField(max_length=255, blank=True, null=True)
    isoelectric_point_reference = models.CharField(max_length=255, blank=True, null=True)
    melting_point_reference = models.CharField(max_length=255, blank=True, null=True)
    moldb_alogps_logp = models.CharField(max_length=255, blank=True, null=True)
    moldb_logp = models.CharField(max_length=255, blank=True, null=True)
    moldb_alogps_logs = models.CharField(max_length=255, blank=True, null=True)
    moldb_smiles = models.TextField(blank=True, null=True)
    moldb_pka = models.CharField(max_length=255, blank=True, null=True)
    moldb_formula = models.CharField(max_length=255, blank=True, null=True)
    moldb_average_mass = models.CharField(max_length=255, blank=True, null=True)
    moldb_inchi = models.TextField(blank=True, null=True)
    moldb_mono_mass = models.CharField(max_length=255, blank=True, null=True)
    moldb_inchikey = models.CharField(max_length=255, blank=True, null=True)
    moldb_alogps_solubility = models.CharField(max_length=255, blank=True, null=True)
    moldb_id = models.IntegerField(blank=True, null=True)
    moldb_iupac = models.TextField(blank=True, null=True)
    structure_source = models.CharField(max_length=255, blank=True, null=True)
    duplicate_id = models.CharField(max_length=255, blank=True, null=True)
    old_dfc_id = models.CharField(max_length=255, blank=True, null=True)
    dfc_name = models.TextField(blank=True, null=True)
    compound_source = models.CharField(max_length=255, blank=True, null=True)
    flavornet_id = models.CharField(max_length=255, blank=True, null=True)
    goodscent_id = models.CharField(max_length=255, blank=True, null=True)
    superscent_id = models.CharField(max_length=255, blank=True, null=True)
    phenolexplorer_metabolite_id = models.IntegerField(blank=True, null=True)
    kingdom = models.CharField(max_length=255, blank=True, null=True)
    superklass = models.CharField(max_length=255, blank=True, null=True)
    klass = models.CharField(max_length=255, blank=True, null=True)
    subklass = models.CharField(max_length=255, blank=True, null=True)
    direct_parent = models.CharField(max_length=255, blank=True, null=True)
    molecular_framework = models.CharField(max_length=255, blank=True, null=True)
    chembl_id = models.CharField(max_length=255, blank=True, null=True)
    chemspider_id = models.CharField(max_length=255, blank=True, null=True)
    meta_cyc_id = models.CharField(max_length=255, blank=True, null=True)
    foodcomex = models.IntegerField(blank=True, null=True)
    phytohub_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'compounds'
        unique_together = (('name', 'public_id'),)


class CompoundsEnzymes(models.Model):
    compound_id = models.IntegerField()
    enzyme_id = models.IntegerField()
    citations = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'compounds_enzymes'
        unique_together = (('compound_id', 'enzyme_id'),)


class CompoundsFlavors(models.Model):
    compound_id = models.IntegerField()
    flavor_id = models.IntegerField()
    citations = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    source_id = models.IntegerField(blank=True, null=True)
    source_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'compounds_flavors'
        unique_together = (('compound_id', 'flavor_id'),)


class CompoundsHealthEffects(models.Model):
    compound_id = models.IntegerField()
    health_effect_id = models.IntegerField()
    orig_health_effect_name = models.CharField(max_length=255, blank=True, null=True)
    orig_compound_name = models.CharField(max_length=255, blank=True, null=True)
    orig_citation = models.TextField(blank=True, null=True)
    citation = models.TextField()
    citation_type = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    source_id = models.IntegerField(blank=True, null=True)
    source_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'compounds_health_effects'


class CompoundsPathways(models.Model):
    compound = models.ForeignKey(Compounds, models.DO_NOTHING, blank=True, null=True)
    pathway = models.ForeignKey('Pathways', models.DO_NOTHING, blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'compounds_pathways'


class Contents(models.Model):
    source_id = models.IntegerField(blank=True, null=True)
    source_type = models.CharField(max_length=255, blank=True, null=True)
    food_id = models.IntegerField()
    orig_food_id = models.CharField(max_length=255, blank=True, null=True)
    orig_food_common_name = models.CharField(max_length=255, blank=True, null=True)
    orig_food_scientific_name = models.CharField(max_length=255, blank=True, null=True)
    orig_food_part = models.CharField(max_length=255, blank=True, null=True)
    orig_source_id = models.CharField(max_length=255, blank=True, null=True)
    orig_source_name = models.CharField(max_length=255, blank=True, null=True)
    orig_content = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)
    orig_min = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)
    orig_max = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)
    orig_unit = models.CharField(max_length=255, blank=True, null=True)
    orig_citation = models.TextField(blank=True, null=True)
    citation = models.TextField()
    citation_type = models.CharField(max_length=255)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    orig_method = models.CharField(max_length=255, blank=True, null=True)
    orig_unit_expression = models.CharField(max_length=255, blank=True, null=True)
    standard_content = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contents'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Enzymes(models.Model):
    name = models.CharField(unique=True, max_length=255)
    gene_name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    go_classification = models.TextField(blank=True, null=True)
    general_function = models.TextField(blank=True, null=True)
    specific_function = models.TextField(blank=True, null=True)
    pathway = models.TextField(blank=True, null=True)
    reaction = models.TextField(blank=True, null=True)
    cellular_location = models.CharField(max_length=255, blank=True, null=True)
    signals = models.TextField(blank=True, null=True)
    transmembrane_regions = models.TextField(blank=True, null=True)
    molecular_weight = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)
    theoretical_pi = models.DecimalField(max_digits=15, decimal_places=9, blank=True, null=True)
    locus = models.CharField(max_length=255, blank=True, null=True)
    chromosome = models.CharField(max_length=255, blank=True, null=True)
    uniprot_name = models.CharField(unique=True, max_length=255, blank=True, null=True)
    uniprot_id = models.CharField(unique=True, max_length=100, blank=True, null=True)
    pdb_id = models.CharField(unique=True, max_length=10, blank=True, null=True)
    genbank_protein_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    genbank_gene_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    genecard_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    genatlas_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    hgnc_id = models.CharField(unique=True, max_length=20, blank=True, null=True)
    hprd_id = models.CharField(unique=True, max_length=255, blank=True, null=True)
    organism = models.CharField(max_length=255, blank=True, null=True)
    general_citations = models.TextField(blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enzymes'


class Flavors(models.Model):
    name = models.CharField(unique=True, max_length=255)
    flavor_group = models.CharField(max_length=255, blank=True, null=True)
    category = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'flavors'


class FoodTaxonomies(models.Model):
    food_id = models.IntegerField(blank=True, null=True)
    ncbi_taxonomy_id = models.IntegerField(blank=True, null=True)
    classification_name = models.CharField(max_length=255, blank=True, null=True)
    classification_order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'food_taxonomies'


class FoodcomexCompoundProviders(models.Model):
    foodcomex_compound_id = models.IntegerField()
    provider_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'foodcomex_compound_providers'


class FoodcomexCompounds(models.Model):
    compound_id = models.IntegerField()
    origin = models.CharField(max_length=255, blank=True, null=True)
    storage_form = models.CharField(max_length=255, blank=True, null=True)
    maximum_quantity = models.CharField(max_length=255, blank=True, null=True)
    storage_condition = models.CharField(max_length=255, blank=True, null=True)
    contact_name = models.CharField(max_length=255, blank=True, null=True)
    contact_address = models.TextField(blank=True, null=True)
    contact_email = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    export = models.IntegerField(blank=True, null=True)
    purity = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    spectra_details = models.CharField(max_length=255, blank=True, null=True)
    delivery_time = models.CharField(max_length=255, blank=True, null=True)
    stability = models.CharField(max_length=255, blank=True, null=True)
    admin_user_id = models.IntegerField(blank=True, null=True)
    public_id = models.CharField(max_length=255)
    cas_number = models.CharField(max_length=255, blank=True, null=True)
    taxonomy_class = models.CharField(max_length=255, blank=True, null=True)
    taxonomy_family = models.CharField(max_length=255, blank=True, null=True)
    experimental_logp = models.CharField(max_length=255, blank=True, null=True)
    experimental_solubility = models.CharField(max_length=255, blank=True, null=True)
    melting_point = models.CharField(max_length=255, blank=True, null=True)
    food_of_origin = models.CharField(max_length=255, blank=True, null=True)
    production_method_reference_text = models.TextField(blank=True, null=True)
    production_method_reference_file_name = models.CharField(max_length=255, blank=True, null=True)
    production_method_reference_content_type = models.CharField(max_length=255, blank=True, null=True)
    production_method_reference_file_size = models.IntegerField(blank=True, null=True)
    production_method_reference_updated_at = models.DateTimeField(blank=True, null=True)
    elemental_formula = models.CharField(max_length=255, blank=True, null=True)
    minimum_quantity = models.CharField(max_length=255, blank=True, null=True)
    quantity_units = models.CharField(max_length=255, blank=True, null=True)
    available_spectra = models.TextField(blank=True, null=True)
    storage_conditions = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'foodcomex_compounds'


class Foods(models.Model):
    name = models.CharField(unique=True, max_length=255)
    name_scientific = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    itis_id = models.CharField(max_length=255, blank=True, null=True)
    wikipedia_id = models.CharField(max_length=255, blank=True, null=True)
    picture_file_name = models.CharField(max_length=255, blank=True, null=True)
    picture_content_type = models.CharField(max_length=255, blank=True, null=True)
    picture_file_size = models.IntegerField(blank=True, null=True)
    picture_updated_at = models.DateTimeField(blank=True, null=True)
    legacy_id = models.IntegerField(blank=True, null=True)
    food_group = models.CharField(max_length=255, blank=True, null=True)
    food_subgroup = models.CharField(max_length=255, blank=True, null=True)
    food_type = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    export_to_afcdb = models.IntegerField()
    category = models.CharField(max_length=255, blank=True, null=True)
    ncbi_taxonomy_id = models.IntegerField(blank=True, null=True)
    export_to_foodb = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'foods'


class HealthEffects(models.Model):
    name = models.CharField(unique=True, max_length=255)
    description = models.TextField(blank=True, null=True)
    chebi_name = models.CharField(max_length=255, blank=True, null=True)
    chebi_id = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    chebi_definition = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'health_effects'


class Nutrients(models.Model):
    legacy_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=255)
    public_id = models.CharField(unique=True, max_length=9)
    name = models.CharField(unique=True, max_length=255)
    export = models.IntegerField(blank=True, null=True)
    state = models.CharField(max_length=255, blank=True, null=True)
    annotation_quality = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    wikipedia_id = models.CharField(max_length=255, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    dfc_id = models.CharField(max_length=255, blank=True, null=True)
    duke_id = models.CharField(max_length=255, blank=True, null=True)
    eafus_id = models.CharField(max_length=255, blank=True, null=True)
    dfc_name = models.TextField(blank=True, null=True)
    compound_source = models.CharField(max_length=255, blank=True, null=True)
    metabolism = models.TextField(blank=True, null=True)
    synthesis_citations = models.TextField(blank=True, null=True)
    general_citations = models.TextField(blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nutrients'
        unique_together = (('name', 'public_id'),)


class Pathways(models.Model):
    smpdb_id = models.CharField(max_length=255, blank=True, null=True)
    kegg_map_id = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pathways'


class References(models.Model):
    ref_type = models.CharField(max_length=255, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    pubmed_id = models.CharField(max_length=255, blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    creator_id = models.IntegerField(blank=True, null=True)
    updater_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    source_id = models.IntegerField(blank=True, null=True)
    source_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'references'
