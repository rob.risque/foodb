from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('foods/<int:food_id>/', views.food_detail, name='food_detail'),
    path('compounds/<int:compound_id>/', views.compound_detail, name='compound_detail'),
    path('nutrients/<int:nutrient_id>/', views.nutrient_detail, name='nutrient_detail'),
    path('search/', views.search_results, name='search_results')
]
