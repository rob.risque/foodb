from django.db.models import Avg, Max
from django.shortcuts import render

from .models import Foods, Contents, Nutrients, Compounds


def index(request):
    # SELECT * from Foods;
    foods = Foods.objects.all()
    context = {'food_list': foods}
    return render(request, 'foodquery/index.html', context)

def search_results(request):

    if request.method == 'GET':
        search_query = request.GET.get('search_box', None)

    food_results = Foods.objects.filter(name__icontains=search_query)
    context = {'food_list': food_results}
    return render(request, 'foodquery/search_results.html', context)


def food_detail(request, food_id):
    # SELECT * from Foods WHERE pk = "food_id";
    food = Foods.objects.get(pk=food_id)

    # SELECT orig_source_name, AVG(orig_content) AS "Content Average", MAX(source_id) AS "source_id" FROM Contents
    #   WHERE food_id = food_id
    #   AND source_type = "Nutrient"
    #   AND citation = "USDA"
    #   GROUP BY orig_source_name
    #   ORDER BY origin_content__avg DESC;
    nutrients = Contents.objects \
        .filter(food_id=food_id) \
        .filter(source_type='Nutrient') \
        .filter(citation='USDA') \
        .values('orig_source_name').annotate(Avg('orig_content')).annotate(Max('source_id')) \
        .order_by('-orig_content__avg')

    # SELECT orig_source_name, AVG(orig_content) AS "Content Average", MAX(source_id) AS "source_id" FROM Contents
    #   WHERE food_id = food_id
    #   AND source_type = "Compound"
    #   AND citation = "USDA"
    #   GROUP BY orig_source_name
    #   ORDER BY origin_content__avg DESC;
    compounds = Contents.objects \
        .filter(food_id=food_id) \
        .filter(source_type='Compound') \
        .filter(citation='USDA') \
        .values('orig_source_name').annotate(Avg('orig_content')).annotate(Max('source_id')) \
        .order_by('-orig_content__avg')

    context = {
        'food': food,
        'nutrients': nutrients,
        'compounds': compounds
    }

    return render(request, 'foodquery/food_detail.html', context)


def compound_detail(request, compound_id):
    # SELECT * FROM Compounds WHERE id = compound_id;
    compound = Compounds.objects.get(id=compound_id)

    context = {
        'compound': compound
    }

    return render(request, 'foodquery/compound_detail.html', context)


def nutrient_detail(request, nutrient_id):
    # SELECT * FROM Compounds WHERE id = nutrient_id;
    nutrient = Nutrients.objects.get(id=nutrient_id)

    context = {
        'nutrient': nutrient
    }

    return render(request, 'foodquery/nutrient_detail.html', context)
